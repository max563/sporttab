#import sys
import argparse

parser = argparse.ArgumentParser(description='A tutorial of argparse!')
parser.add_argument("--team1", default="")
parser.add_argument("--team2", default="")
parser.add_argument("--leftGoal", default="")
parser.add_argument("--period", default="")
parser.add_argument("--rightGoal", default="")
parser.add_argument("--mainTimer", default="")
parser.add_argument("--lupPenaltyNum", default="")
parser.add_argument("--lupPenaltyTime", default="")
parser.add_argument("--ldnPenaltyNum", default="")
parser.add_argument("--ldnPenaltyTime", default="")
parser.add_argument("--rupPenaltyNum", default="")
parser.add_argument("--rupPenaltyTime", default="")
parser.add_argument("--rdnPenaltyNum", default="")
parser.add_argument("--rdnPenaltyTime", default="")

args = parser.parse_args()

team1 = args.team1
team2 = args.team2
leftGoal = args.leftGoal
period = args.period
rightGoal = args.rightGoal
mainTimer = args.mainTimer
lupPenaltyNum = args.lupPenaltyNum
lupPenaltyTime = args.lupPenaltyTime
ldnPenaltyNum = args.ldnPenaltyNum
ldnPenaltyTime = args.ldnPenaltyTime
rupPenaltyNum = args.rupPenaltyNum
rupPenaltyTime = args.rupPenaltyTime
rdnPenaltyNum = args.rdnPenaltyNum
rdnPenaltyTime = args.rdnPenaltyTime

f = open('text.txt', 'w', encoding='utf-8')
f.write(team1+" "+leftGoal+" - "+rightGoal+" "+team2+"\n")
f.write("Период:"+period+" Время:"+mainTimer)
f.close