import java.util.Timer;
import java.util.TimerTask;

public class ValuesToFile {
	
	SportTabMainWindow mainWindow;
	Timer timer;
	
	public ValuesToFile(SportTabMainWindow mainWindow) {
		this.mainWindow = mainWindow;
	}
	
	public void start() {
		timer = new Timer("Timer");
		MTimerTask timerTask = new MTimerTask();
		timer.scheduleAtFixedRate(timerTask, 1000l, 1000l);
	}
	
	public void stop() {
		try {
		timer.cancel();
		}catch(NullPointerException e){}
	}
	
	class MTimerTask extends TimerTask{
		@Override
		public void run() {
			String[] teams = {"",""};
			teams = mainWindow.teams;
			String[] cmd = {
					"python"
					,"1.py"
					,"--team1="+teams[0]
					,"--team2="+teams[1]
		            ,"--leftGoal="+mainWindow.leftGoal.getText()
		            ,"--period="+mainWindow.lblPeriod.getText()
		            ,"--rightGoal="+mainWindow.rightGoal.getText()
		            ,"--mainTimer="+mainWindow.lblMainTimer.getText()
		            ,"--lupPenaltyNum="+mainWindow.textField_1.getText()
		            ,"--lupPenaltyTime="+mainWindow.label_10.getText()
		            ,"--ldnPenaltyNum="+mainWindow.textField_3.getText()
		            ,"--ldnPenaltyTime="+mainWindow.label_13.getText()
		            ,"--rupPenaltyNum="+mainWindow.textField_2.getText()
		            ,"--rupPenaltyTime="+mainWindow.label_11.getText()
		            ,"--rdnPenaltyNum="+mainWindow.textField_4.getText()
		            ,"--rdnPenaltyTime="+mainWindow.label_15.getText()
			};
			try
	        {
	            Runtime r = Runtime.getRuntime();
	            Process p = r.exec(cmd);
	            //System.out.println(p.info());
	        }
	        catch (Exception e)
	        {
	        String cause = e.getMessage();
	        if (cause.equals("python: not found"))
	            System.out.println("No python interpreter found.");
	        }
			
			//System.out.println(mainWindow.lblMainTimer.getText());
			
		}
	}

}
