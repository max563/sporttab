import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;

import java.io.File;
import java.util.Enumeration;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class Sport {

	static SportTabCls sportTab;
	static SerialPort serialPort = null;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (!checkForLibrares())
			return;

//		Scanner in = new Scanner(System.in);
//		System.out.println("������� COM ���� �� ������ ������ COM4:");
//		String defaultPort = in.nextLine();
//		// in.close();
//		serialPort = getSerialPort(defaultPort);
//		if (serialPort == null) {
//			// System.out.println("���� " + defaultPort + " �� ������.");
//			return;
//		}
//		sportTab = new SportTabCls(serialPort);
//		System.out
//				.println("������� ������ ������������ �� ����� �� 24 �������:");
//		String toStringTab = in.nextLine();
//		in.close();
//		if (toStringTab == "exit") {
//			sportTab.close();
//			return;
//		}
//		System.out.println("otpravka 1");
//		sportTab.setToString(toStringTab);
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e) {
//		}
//
//		sportTab.close();

	}

	private static boolean checkForLibrares() {
		String pathJre = System.getProperty("java.home");
		File file1 = new File(pathJre + "/bin/rxtxSerial.dll");
		boolean existsRxtxSerial = file1.exists();
		// System.out.println(file1+" "+existsRxtxSerial);

		File file2 = new File(pathJre + "/lib/ext/RXTXcomm.jar");
		boolean existsRXTXCommJ = file2.exists();
		// System.out.println(file2+" "+existsRxtxSerial);
		if (!existsRxtxSerial | !existsRXTXCommJ) {
			String message = "���������� ���������� ����������(�) :\n";
			if (!existsRxtxSerial) {
				message += file1.getAbsolutePath() + "\n";
			}
			if (!existsRXTXCommJ) {
				message += file2.getAbsolutePath() + "\n";
			}
			message += "������ ���������� ����� �������\n� �����: http://rxtx.qbang.org/\n� �������: downloads, � ����������� � ��������� �����.\n";
			message += "��� ���������� �������������� ������: RXTX-2.1-7\n������ ������ �������� ���� ��������.";
			JOptionPane.showMessageDialog(null, message);
			return false;
		}
		return true;
	}

	private static SerialPort getSerialPort(String defaultPort) {
		Enumeration portList;
		CommPortIdentifier portId;

		portList = CommPortIdentifier.getPortIdentifiers();
		while (portList.hasMoreElements()) {
			portId = (CommPortIdentifier) portList.nextElement();
			if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
				if (portId.getName().equals(defaultPort)) {
					System.out.println("������ ����:" + defaultPort);
					try {
						return (SerialPort) portId.open("SimpleWrite", 2000);
					} catch (PortInUseException e) {
						System.out.println("���� ��� ������������");
						continue;
					}
				}
			}
		}
		return null;
	}

}
