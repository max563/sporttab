import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.SplashScreen;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;
import java.util.prefs.Preferences;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
import javax.swing.JCheckBox;
import javax.swing.AbstractAction;
import javax.swing.Action;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class SportTabMainWindow implements SportTabCls.SportTabClsEvents {

	private URL urlSound = this.getClass().getResource("/res/sound/sirena.wav");
	private AudioClip clip = null;
	private JFrame frame;
	private JTextField textField;
	public JLabel label, label_1, leftGoal, rightGoal, label_4, lblPeriod, lblMainTimer, label_10, label_11,
			lblTeams, label_13, label_15;
	private JComboBox<String> comboBox_1, comboBox, comboBox_3, comboBox_4, comboBox_5, comboBox_2;
	private JMenuBar menuBar;
	private String[] initPenaltyNumber = new String[101];
	private String[] initPenaltyTime = new String[] { "", "01:00", "01:30", "02:00", "03:00", "04:00",
			"05:00", "10:00" };
	private JButton button_1, button_2, button_3, button_4, button_5, button_6, button_8, button_13;
	private JSpinner spinner;
	private JToggleButton toggleButton;
	private SportTabCls sportTab = null;
	private ValuesToFile valuesToFile=new ValuesToFile(this);
	//private Preferences userPrefs;
	private Properties properties;
	private File propertiesFile;
	public JTextField textField_1;
	public JTextField textField_2;
	public JTextField textField_3;
	public JTextField textField_4;
	int percentSplash=0;
	static SplashScreen splash=null;
	//private final Action action = new SwingAction();
	public String[] teams= {"",""};
	Vector<String> comPorts;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		splash = SplashScreen.getSplashScreen();
        if (splash == null) {
            System.out.println("SplashScreen.getSplashScreen() returned null");
            return;
        }
        final Graphics2D g = splash.createGraphics();
        if (g == null) {
            System.out.println("g is null");
            return;
        }
        Thread t=new Thread(){       	
			@Override
			public void run() {				
				for(int i=0; true; i++) {
					
		            renderSplashFrame(g, i);
		            splash.update();
		            try {
		                Thread.sleep(90);
		            }
		            catch(InterruptedException e) {
		            }   
		        }
			}
        };
        t.start();        
        
		EventQueue.invokeLater(new Runnable() {
			public void run() {

				try {
					SportTabMainWindow window = new SportTabMainWindow();
					System.out.println("show");
					splash.close();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	static void renderSplashFrame(Graphics2D g, int frame) {
        g.setComposite(AlphaComposite.Clear);
        g.fillRect(120,140,200,40);
        g.setPaintMode();
        g.setColor(Color.BLACK);
        g.drawString("�������� "+frame+"%", 120, 150);
    }
	
	public void updateSplash(Graphics2D g,int per){
		renderSplashFrame(g, per);
        splash.update();
	}

	/**
	 * Create the application.
	 */
	public SportTabMainWindow() {
		//userPrefs = Preferences.userRoot().node("SportTab");
		properties = new Properties();
		propertiesFile = new File("config.txt");
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		initPenaltyNumber[0] = "";
		for (int i = 1; i < 101; i++)
			initPenaltyNumber[i] = Integer.toString(i);

		comPorts = getArrayComPorts();
		
		frame = new JFrame();
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				System.out.println("Closing");
				if (sportTab != null)
					sportTab.close();
			}
		});
		frame.setResizable(false);
		frame.setTitle("\u0421\u043F\u043E\u0440\u0442\u0438\u0432\u043D\u043E\u0435 \u0442\u0430\u0431\u043B\u043E");
		frame.getContentPane().setBackground(Color.BLACK);
		frame.getContentPane().setLayout(null);

		label = new JLabel("\u0421\u0442\u0440\u043E\u043A\u0430:");
		label.setFont(new Font("Dialog", Font.BOLD, 12));
		label.setForeground(Color.WHITE);
		label.setBounds(10, 11, 46, 14);
		frame.getContentPane().add(label);

		comboBox = new JComboBox<String>();
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("actionPerformed");
				// userPrefs.put("COMPortToString", comboBox.getSelectedItem()
				// .toString());
				if (sportTab != null) {
					sportTab.close();
					sportTab = null;
				}
				//initSportTabObject();
			}
		});
		comboBox.setModel(new DefaultComboBoxModel<>(comPorts));
		comboBox.setBounds(66, 8, 83, 20);
		frame.getContentPane().add(comboBox);

		label_1 = new JLabel("\u0422\u0430\u0431\u043B\u043E:");
		label_1.setForeground(Color.WHITE);
		label_1.setBounds(196, 11, 46, 14);
		frame.getContentPane().add(label_1);

		comboBox_1 = new JComboBox<String>();
		comboBox_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// userPrefs.put("COMPortToTab", comboBox_1.getSelectedItem()
				// .toString());
				if (sportTab != null) {
					sportTab.close();
					sportTab = null;
				}
				initSportTabObject();
			}
		});
		comboBox_1.setModel(new DefaultComboBoxModel<>(comPorts));
		comboBox_1.setBounds(252, 8, 86, 20);
		frame.getContentPane().add(comboBox_1);

		textField = new JTextField();
		textField.setFont(new Font("Monospaced", Font.PLAIN, 14));
		textField.setBounds(10, 37, 222, 20);
		// ����������� ����� �� ���������� ��������
		textField.setDocument(new PlainDocument() {
			private static final long serialVersionUID = 1L;

			@Override
			public void insertString(int offset, String str, javax.swing.text.AttributeSet attr)
					throws BadLocationException {
				if (str == null)
					return;
				if ((getLength() + str.length()) <= 24) {
					super.insertString(offset, str, attr);
				}
			}
		});
		frame.getContentPane().add(textField);
		textField.setColumns(10);

		JButton button = new JButton("\u041E\u0442\u043E\u0431\u0440\u0430\u0437\u0438\u0442\u044C");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (sportTab != null)
					sportTab.setToString(textField.getText());
				lblTeams.setText(textField.getText());
				//������� ������� � ������ � � ����� � ����� ������ ����� ������� �����
				teams = lblTeams.getText().trim().split(" ", 2);
				//������� � ������ ������� ������� ����� ���������
				teams[1]=teams[1].trim();
				//teams = teams.split(" ", 2);
			}
		});
		button.setMargin(new Insets(2, 5, 2, 5));
		button.setBounds(252, 35, 98, 23);
		frame.getContentPane().add(button);

		lblTeams = new JLabel("");
		lblTeams.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		lblTeams.setFont(new Font("Monospaced", Font.BOLD, 37));
		lblTeams.setForeground(Color.RED);
		lblTeams.setBounds(10, 67, 543, 56);
		frame.getContentPane().add(lblTeams);

		JButton btnX = new JButton("X");
		btnX.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (sportTab != null)
					sportTab.setToString("");
				textField.setText("");
				lblTeams.setText("");
			}
		});
		btnX.setMargin(new Insets(2, 2, 2, 2));
		btnX.setToolTipText("\u041E\u0447\u0438\u0441\u0442\u043A\u0430 \u0441\u0442\u0440\u043E\u043A\u0438");
		btnX.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnX.setBounds(528, 36, 25, 23);
		frame.getContentPane().add(btnX);

		leftGoal = new JLabel("0");
		leftGoal.setHorizontalAlignment(SwingConstants.RIGHT);
		leftGoal.setFont(new Font("Monospaced", Font.BOLD, 80));
		leftGoal.setForeground(Color.RED);
		leftGoal.setBounds(10, 135, 127, 89);
		frame.getContentPane().add(leftGoal);

		rightGoal = new JLabel("0");
		rightGoal.setHorizontalAlignment(SwingConstants.RIGHT);
		rightGoal.setForeground(Color.RED);
		rightGoal.setFont(new Font("Monospaced", Font.BOLD, 80));
		rightGoal.setBounds(391, 134, 135, 89);
		frame.getContentPane().add(rightGoal);

		label_4 = new JLabel("\u041F\u0435\u0440\u0438\u043E\u0434");
		label_4.setForeground(Color.WHITE);
		label_4.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_4.setBounds(238, 151, 78, 23);
		frame.getContentPane().add(label_4);

		lblPeriod = new JLabel("0");
		lblPeriod.setFont(new Font("Monospaced", Font.BOLD, 33));
		lblPeriod.setForeground(Color.RED);
		lblPeriod.setBounds(267, 183, 25, 33);
		frame.getContentPane().add(lblPeriod);

		button_1 = new JButton("+1");
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (Integer.parseInt(leftGoal.getText()) < 19) {
					leftGoal.setText(Integer.toString(Integer.parseInt(leftGoal.getText()) + 1));
					sportTab.setLeftGoal(Integer.parseInt(leftGoal.getText()));
				}
			}
		});
		button_1.setMargin(new Insets(2, 2, 2, 2));
		button_1.setBounds(61, 221, 25, 23);
		frame.getContentPane().add(button_1);

		button_2 = new JButton("+1");
		button_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (Integer.parseInt(lblPeriod.getText()) < 9) {
					lblPeriod.setText(Integer.toString(Integer.parseInt(lblPeriod.getText()) + 1));
					sportTab.setPeriod(Integer.parseInt(lblPeriod.getText()));
				}
			}
		});
		button_2.setMargin(new Insets(2, 2, 2, 2));
		button_2.setBounds(249, 221, 25, 23);
		frame.getContentPane().add(button_2);

		button_3 = new JButton("+1");
		button_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (Integer.parseInt(rightGoal.getText()) < 19) {
					rightGoal.setText(Integer.toString(Integer.parseInt(rightGoal.getText()) + 1));
					sportTab.setRightGoal(Integer.parseInt(rightGoal.getText()));
				}
			}
		});
		button_3.setMargin(new Insets(2, 2, 2, 2));
		button_3.setBounds(442, 221, 25, 23);
		frame.getContentPane().add(button_3);

		button_4 = new JButton("-1");
		button_4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (Integer.parseInt(leftGoal.getText()) > 0) {
					leftGoal.setText(Integer.toString(Integer.parseInt(leftGoal.getText()) - 1));
					sportTab.setLeftGoal(Integer.parseInt(leftGoal.getText()));
				}
			}
		});
		button_4.setMargin(new Insets(2, 2, 2, 2));
		button_4.setBounds(96, 221, 25, 23);
		frame.getContentPane().add(button_4);

		button_5 = new JButton("-1");
		button_5.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (Integer.parseInt(lblPeriod.getText()) > 0) {
					lblPeriod.setText(Integer.toString(Integer.parseInt(lblPeriod.getText()) - 1));
					sportTab.setPeriod(Integer.parseInt(lblPeriod.getText()));
				}
			}
		});
		button_5.setMargin(new Insets(2, 2, 2, 2));
		button_5.setBounds(277, 221, 25, 23);
		frame.getContentPane().add(button_5);

		button_6 = new JButton("-1");
		button_6.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (Integer.parseInt(rightGoal.getText()) > 0) {
					rightGoal.setText(Integer.toString(Integer.parseInt(rightGoal.getText()) - 1));
					sportTab.setRightGoal(Integer.parseInt(rightGoal.getText()));
				}
			}
		});
		button_6.setMargin(new Insets(2, 2, 2, 2));
		button_6.setBounds(477, 221, 25, 23);
		frame.getContentPane().add(button_6);

		lblMainTimer = new JLabel("00:00");
		lblMainTimer.setToolTipText("\u041F\u0440\u0430\u0432\u044B\u043C \u043A\u043B\u0438\u043A\u043E\u043C \u043C\u043E\u0436\u043D\u043E \u0432\u0432\u0435\u0441\u0442\u0438 \u0432\u0440\u0435\u043C\u044F c \u043A\u043E\u0442\u043E\u0440\u043E\u0433\u043E \u0431\u0443\u0434\u0435\u0442 \u0432\u0435\u0441\u0442\u0438\u0441\u044C \u043E\u0442\u0441\u0447\u0451\u0442");
		lblMainTimer.setBorder(new LineBorder(Color.WHITE));
		lblMainTimer.setFont(new Font("Monospaced", Font.BOLD, 50));
		lblMainTimer.setForeground(Color.RED);
		lblMainTimer.setBounds(196, 255, 155, 55);
		JPopupMenu popupMenu0 = new JPopupMenu();
		JMenuItem handInputItem0 = new JMenuItem("������ �������");
		handInputItem0.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int i = showHandTimeInputDialog("����� � �������� �������� ������");
				if (i >= 0) {
					//label_6.setText(secToTimeString(i));
					sportTab.setInitialTime(i);
				}
			}
		});
		popupMenu0.add(handInputItem0);
		lblMainTimer.setComponentPopupMenu(popupMenu0);
		frame.getContentPane().add(lblMainTimer);

		JButton button_7 = new JButton("X");
		button_7.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				sportTab.timerReset();
				toggleButton.setEnabled(true);
				toggleButton.setSelected(false);
			}
		});
		button_7.setToolTipText("\u0421\u0431\u0440\u043E\u0441 \u0442\u0430\u0439\u043C\u0435\u0440\u0430");
		button_7.setMargin(new Insets(2, 2, 2, 2));
		button_7.setFont(new Font("Tahoma", Font.PLAIN, 12));
		button_7.setBounds(353, 265, 25, 23);
		frame.getContentPane().add(button_7);

		JLabel label_7 = new JLabel("\u0428\u0442\u0440\u0430\u0444");
		label_7.setForeground(Color.WHITE);
		label_7.setFont(new Font("Tahoma", Font.BOLD, 15));
		label_7.setBounds(10, 256, 57, 20);
		frame.getContentPane().add(label_7);

		JLabel label_8 = new JLabel("\u0428\u0442\u0440\u0430\u0444");
		label_8.setForeground(Color.WHITE);
		label_8.setFont(new Font("Tahoma", Font.BOLD, 15));
		label_8.setBounds(496, 255, 57, 23);
		frame.getContentPane().add(label_8);

		JLabel label_9 = new JLabel("\u041D\u043E\u043C\u0435\u0440");
		label_9.setForeground(Color.WHITE);
		label_9.setBackground(Color.WHITE);
		label_9.setBounds(10, 289, 37, 16);
		frame.getContentPane().add(label_9);

		comboBox_3 = new JComboBox<String>();
		comboBox_3.setToolTipText("\u041F\u0440\u0430\u0432\u044B\u043C \u043A\u043B\u0438\u043A\u043E\u043C \u043C\u043E\u0436\u043D\u043E \u0432\u0432\u0435\u0441\u0442\u0438 \u0432\u0440\u0435\u043C\u044F \u0432\u0440\u0443\u0447\u043D\u0443\u044E");
		comboBox_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String s = comboBox_3.getSelectedItem().toString();
				label_10.setText(s);
				if (!s.equals("")) {
					String s1, s2;
					s1 = s.substring(0, 2);
					s2 = s.substring(3, 5);
					int min = Integer.parseInt(s1), sec = Integer.parseInt(s2);
					sportTab.setPenaltyTime(1, (min * 60) + sec);
				} else {
					sportTab.setPenaltyTime(1, 0);
					sportTab.clearPenaltyTime(1);
				}
			}
		});
		JPopupMenu popupMenu = new JPopupMenu();
		JMenuItem handInputItem = new JMenuItem("������ �������");
		handInputItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int i = showHandTimeInputDialog("�������� �����");
				if (i >= 0) {
					timePenalty(1, i);
					sportTab.setPenaltyTime(1, i);
				}
			}
		});
		popupMenu.add(handInputItem);
		comboBox_3.setComponentPopupMenu(popupMenu);
		comboBox_3.setModel(new DefaultComboBoxModel<>(initPenaltyTime));
		comboBox_3.setBounds(85, 281, 64, 24);
		frame.getContentPane().add(comboBox_3);

		label_10 = new JLabel("");
		label_10.setBorder(new LineBorder(Color.WHITE));
		label_10.setFont(new Font("Monospaced", Font.BOLD, 29));
		label_10.setForeground(Color.RED);
		label_10.setBounds(85, 308, 91, 33);
		frame.getContentPane().add(label_10);

		button_8 = new JButton("X");
		button_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textField_1.setText("");
				comboBox_3.setSelectedItem("");
			}
		});
		button_8.setToolTipText("\u0421\u0431\u0440\u043E\u0441 \u0448\u0442\u0440\u0430\u0444\u043D\u043E\u0433\u043E \u043D\u043E\u043C\u0435\u0440\u0430 \u0438 \u0435\u0433\u043E \u0432\u0440\u0435\u043C\u0435\u043D\u0438");
		button_8.setMargin(new Insets(2, 2, 2, 2));
		button_8.setFont(new Font("Tahoma", Font.PLAIN, 12));
		button_8.setBounds(159, 281, 17, 24);
		frame.getContentPane().add(button_8);

		button_13 = new JButton("\u041F\u0440\u044F\u043C\u043E\u0439");
		button_13.setBounds(211, 425, 127, 26);
		frame.getContentPane().add(button_13);

		JSeparator separator = new JSeparator();
		separator.setBounds(10, 345, 166, 8);
		frame.getContentPane().add(separator);

		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(387, 345, 166, 8);
		frame.getContentPane().add(separator_1);

		spinner = new JSpinner();
		spinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				sportTab.setPeriodTime(((int) spinner.getValue()) * 60);
			}
		});
		spinner.setToolTipText("\u0412\u0440\u0435\u043C\u044F \u043F\u0435\u0440\u0438\u043E\u0434\u0430 \u0432 \u043C\u0438\u043D\u0443\u0442\u0430\u0445");
		spinner.setModel(new SpinnerNumberModel(20, 1, 20, 1));
		spinner.setBounds(109, 431, 51, 20);
		frame.getContentPane().add(spinner);

		JLabel label_17 = new JLabel(
				"\u0412\u0440\u0435\u043C\u044F \u043F\u0435\u0440\u0438\u043E\u0434\u0430");
		label_17.setForeground(Color.WHITE);
		label_17.setBounds(10, 433, 98, 16);
		frame.getContentPane().add(label_17);

		JButton button_14 = new JButton("\u041E\u0447\u0438\u0441\u0442\u043A\u0430");
		button_14.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				leftGoal.setText("0");
				sportTab.setLeftGoal(Integer.parseInt(leftGoal.getText()));
				rightGoal.setText("0");
				sportTab.setRightGoal(Integer.parseInt(rightGoal.getText()));
				lblPeriod.setText("0");
				sportTab.setPeriod(Integer.parseInt(lblPeriod.getText()));
				textField_1.setText("");
				comboBox_3.setSelectedItem("");
				textField_3.setText("");
				comboBox_4.setSelectedItem("");
				textField_2.setText("");
				comboBox_2.setSelectedItem("");
				textField_4.setText("");
				comboBox_5.setSelectedItem("");
				sportTab.timerReset();
				toggleButton.setEnabled(true);
				toggleButton.setSelected(false);
			}
		});
		button_14
				.setToolTipText("\u0421\u0431\u0440\u043E\u0441 \u0442\u0430\u0431\u043B\u043E \u0432 \u043F\u0435\u0440\u0432\u043E\u043D\u0430\u0447\u0430\u043B\u044C\u043D\u043E\u0435 \u0437\u043D\u0430\u0447\u0435\u043D\u0438\u0435");
		button_14.setBounds(455, 428, 98, 26);
		frame.getContentPane().add(button_14);
		frame.setBounds(100, 100, 579, 524);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenu menu = new JMenu("\u041D\u0430\u0441\u0442\u0440\u043E\u0439\u043A\u0438");
		menuBar.add(menu);

		JMenuItem menuItem_2 = new JMenuItem("\u041D\u0430\u0441\u0442\u0440\u043E\u0439\u043A\u0438");
		menu.add(menuItem_2);

		JMenu menu_1 = new JMenu("\u0421\u043F\u0440\u0430\u0432\u043A\u0430");
		menuBar.add(menu_1);

		JMenuItem menuItem = new JMenuItem("\u0421\u043F\u0440\u0430\u0432\u043A\u0430");
		menu_1.add(menuItem);

		JMenuItem menuItem_1 = new JMenuItem("\u041E \u043F\u0440\u043E\u0433\u0440\u0430\u043C\u043C\u0435");
		menu_1.add(menuItem_1);

		// ������ �������� � ��������� �������� � combo box-�
		/*
		comboBox.setSelectedItem(userPrefs.get("COMPortToString", ""));
		comboBox_1.setSelectedItem(userPrefs.get("COMPortToTab", ""));
		*/
		try {
			properties.load(new FileInputStream(propertiesFile));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		comboBox.setSelectedItem(properties.getProperty("COMPortToString", ""));
		comboBox_1.setSelectedItem(properties.getProperty("COMPortToTab", ""));

		toggleButton = new JToggleButton("\u041F\u0443\u0441\u043A");
		toggleButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (toggleButton.isSelected()) {
					// ��������� ������� �������, ����� ��������� �������� �����
					// �� ����� ����
					sportTab.setPeriodTime(((int) spinner.getValue()) * 60);
					sportTab.timerStart();
				} else {
					sportTab.timerStop();

				}
			}
		});
		toggleButton.setBounds(211, 322, 127, 87);
		frame.getContentPane().add(toggleButton);

		textField_1 = new JTextField();
		textField_1.setDocument(new PlainDocument() {
			private static final long serialVersionUID = 1L;

			@Override
			public void insertString(int offset, String str, javax.swing.text.AttributeSet attr)
					throws BadLocationException {
				if (str == null)
					return;
				if ((getLength() + str.length()) <= 2) {
					// ����� ���� ������ �����
					if (checkString(str)) {
						super.insertString(offset, str, attr);
					}
				}
			}
		});
		textField_1.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				String text = textField_1.getText();
				if (!text.equals("")) {
					sportTab.setPenaltyNumber(1, Integer.parseInt(text));
				} else
					sportTab.setPenaltyNumber(1, -1);
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				sportTab.setPenaltyNumber(1, Integer.parseInt(textField_1.getText()));
			}

			@Override
			public void changedUpdate(DocumentEvent arg0) {
				System.out.println("changedUpdate");
			}
		});
		textField_1.setCaretColor(Color.RED);
		textField_1.setForeground(Color.RED);
		textField_1.setBackground(Color.BLACK);
		textField_1.setFont(new Font("Monospaced", Font.BOLD, 24));
		textField_1.setBounds(10, 308, 67, 33);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);

		JButton button_9 = new JButton("\u0421\u043E\u0445\u0440\u0430\u043D\u0438\u0442\u044C");
		button_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				properties.setProperty("COMPortToString", comboBox.getSelectedItem().toString());
				properties.setProperty("COMPortToTab", comboBox_1.getSelectedItem().toString());
				try {
					properties.store(new FileOutputStream(propertiesFile), null);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				/*
				userPrefs.put("COMPortToString", comboBox.getSelectedItem().toString());
				userPrefs.put("COMPortToTab", comboBox_1.getSelectedItem().toString());
				*/
				JOptionPane.showMessageDialog(null, "��������� COM ������ ���������");
			}
		});
		button_9.setBounds(350, 7, 98, 21);
		frame.getContentPane().add(button_9);

		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(10, 31, 543, 2);
		frame.getContentPane().add(separator_2);

		label_11 = new JLabel("");
		label_11.setForeground(Color.RED);
		label_11.setFont(new Font("Monospaced", Font.BOLD, 29));
		label_11.setBorder(new LineBorder(Color.WHITE));
		label_11.setBounds(462, 308, 91, 33);
		frame.getContentPane().add(label_11);

		textField_2 = new JTextField();
		textField_2.setDocument(new PlainDocument() {
			private static final long serialVersionUID = 1L;

			@Override
			public void insertString(int offset, String str, javax.swing.text.AttributeSet attr)
					throws BadLocationException {
				if (str == null)
					return;
				if ((getLength() + str.length()) <= 2) {
					// ����� ���� ������ �����
					if (checkString(str)) {
						super.insertString(offset, str, attr);
					}
				}
			}
		});
		textField_2.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				String text = textField_2.getText();
				if (!text.equals("")) {
					sportTab.setPenaltyNumber(3, Integer.parseInt(text));
				} else
					sportTab.setPenaltyNumber(3, -1);
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				sportTab.setPenaltyNumber(3, Integer.parseInt(textField_2.getText()));
			}

			@Override
			public void changedUpdate(DocumentEvent arg0) {
				System.out.println("changedUpdate");
			}
		});
		textField_2.setForeground(Color.RED);
		textField_2.setFont(new Font("Monospaced", Font.BOLD, 24));
		textField_2.setColumns(10);
		textField_2.setCaretColor(Color.RED);
		textField_2.setBackground(Color.BLACK);
		textField_2.setBounds(387, 308, 67, 33);
		frame.getContentPane().add(textField_2);

		JLabel label_12 = new JLabel("\u041D\u043E\u043C\u0435\u0440");
		label_12.setForeground(Color.WHITE);
		label_12.setBackground(Color.WHITE);
		label_12.setBounds(387, 289, 37, 16);
		frame.getContentPane().add(label_12);

		comboBox_2 = new JComboBox<String>();
		comboBox_2.setToolTipText("\u041F\u0440\u0430\u0432\u044B\u043C \u043A\u043B\u0438\u043A\u043E\u043C \u043C\u043E\u0436\u043D\u043E \u0432\u0432\u0435\u0441\u0442\u0438 \u0432\u0440\u0435\u043C\u044F \u0432\u0440\u0443\u0447\u043D\u0443\u044E");
		comboBox_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String s = comboBox_2.getSelectedItem().toString();
				label_11.setText(s);
				if (!s.equals("")) {
					String s1, s2;
					s1 = s.substring(0, 2);
					s2 = s.substring(3, 5);
					int min = Integer.parseInt(s1), sec = Integer.parseInt(s2);
					sportTab.setPenaltyTime(3, (min * 60) + sec);
				} else {
					sportTab.setPenaltyTime(3, 0);
					sportTab.clearPenaltyTime(3);
				}
			}
		});
		JPopupMenu popupMenu3 = new JPopupMenu();
		JMenuItem handInputItem3 = new JMenuItem("������ �������");
		handInputItem3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int i = showHandTimeInputDialog("�������� �����");
				if (i >= 0) {
					timePenalty(3, i);
					sportTab.setPenaltyTime(3, i);
				}
			}
		});
		popupMenu3.add(handInputItem3);
		comboBox_2.setComponentPopupMenu(popupMenu3);
		comboBox_2.setModel(new DefaultComboBoxModel<>(initPenaltyTime));
		comboBox_2.setBounds(462, 281, 64, 24);
		frame.getContentPane().add(comboBox_2);

		JButton button_10 = new JButton("X");
		button_10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField_2.setText("");
				comboBox_2.setSelectedItem("");
			}
		});
		button_10
				.setToolTipText("\u0421\u0431\u0440\u043E\u0441 \u0448\u0442\u0440\u0430\u0444\u043D\u043E\u0433\u043E \u043D\u043E\u043C\u0435\u0440\u0430 \u0438 \u0435\u0433\u043E \u0432\u0440\u0435\u043C\u0435\u043D\u0438");
		button_10.setMargin(new Insets(2, 2, 2, 2));
		button_10.setFont(new Font("Tahoma", Font.PLAIN, 12));
		button_10.setBounds(536, 281, 17, 24);
		frame.getContentPane().add(button_10);

		label_13 = new JLabel("");
		label_13.setForeground(Color.RED);
		label_13.setFont(new Font("Monospaced", Font.BOLD, 29));
		label_13.setBorder(new LineBorder(Color.WHITE));
		label_13.setBounds(85, 386, 91, 33);
		frame.getContentPane().add(label_13);

		textField_3 = new JTextField();
		textField_3.setDocument(new PlainDocument() {
			private static final long serialVersionUID = 1L;

			@Override
			public void insertString(int offset, String str, javax.swing.text.AttributeSet attr)
					throws BadLocationException {
				if (str == null)
					return;
				if ((getLength() + str.length()) <= 2) {
					// ����� ���� ������ �����
					if (checkString(str)) {
						super.insertString(offset, str, attr);
					}
				}
			}
		});
		textField_3.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				String text = textField_3.getText();
				if (!text.equals("")) {
					sportTab.setPenaltyNumber(2, Integer.parseInt(text));
				} else
					sportTab.setPenaltyNumber(2, -1);
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				sportTab.setPenaltyNumber(2, Integer.parseInt(textField_3.getText()));
			}

			@Override
			public void changedUpdate(DocumentEvent arg0) {
				System.out.println("changedUpdate");
			}
		});
		textField_3.setForeground(Color.RED);
		textField_3.setFont(new Font("Monospaced", Font.BOLD, 24));
		textField_3.setColumns(10);
		textField_3.setCaretColor(Color.RED);
		textField_3.setBackground(Color.BLACK);
		textField_3.setBounds(10, 386, 67, 33);
		frame.getContentPane().add(textField_3);

		JLabel label_14 = new JLabel("\u041D\u043E\u043C\u0435\u0440");
		label_14.setForeground(Color.WHITE);
		label_14.setBackground(Color.WHITE);
		label_14.setBounds(10, 367, 37, 16);
		frame.getContentPane().add(label_14);

		comboBox_4 = new JComboBox<String>();
		comboBox_4.setToolTipText("\u041F\u0440\u0430\u0432\u044B\u043C \u043A\u043B\u0438\u043A\u043E\u043C \u043C\u043E\u0436\u043D\u043E \u0432\u0432\u0435\u0441\u0442\u0438 \u0432\u0440\u0435\u043C\u044F \u0432\u0440\u0443\u0447\u043D\u0443\u044E");
		comboBox_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String s = comboBox_4.getSelectedItem().toString();
				label_13.setText(s);
				if (!s.equals("")) {
					String s1, s2;
					s1 = s.substring(0, 2);
					s2 = s.substring(3, 5);
					int min = Integer.parseInt(s1), sec = Integer.parseInt(s2);
					sportTab.setPenaltyTime(2, (min * 60) + sec);
				} else {
					sportTab.setPenaltyTime(2, 0);
					sportTab.clearPenaltyTime(2);
				}
			}
		});
		JPopupMenu popupMenu2 = new JPopupMenu();
		JMenuItem handInputItem2 = new JMenuItem("������ �������");
		handInputItem2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int i = showHandTimeInputDialog("�������� �����");
				if (i >= 0) {
					timePenalty(2, i);
					sportTab.setPenaltyTime(2, i);
				}
			}
		});
		popupMenu2.add(handInputItem2);
		comboBox_4.setComponentPopupMenu(popupMenu2);
		comboBox_4.setModel(new DefaultComboBoxModel<>(initPenaltyTime));
		comboBox_4.setBounds(85, 359, 64, 24);
		frame.getContentPane().add(comboBox_4);

		JButton button_11 = new JButton("X");
		button_11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textField_3.setText("");
				comboBox_4.setSelectedItem("");
			}
		});
		button_11
				.setToolTipText("\u0421\u0431\u0440\u043E\u0441 \u0448\u0442\u0440\u0430\u0444\u043D\u043E\u0433\u043E \u043D\u043E\u043C\u0435\u0440\u0430 \u0438 \u0435\u0433\u043E \u0432\u0440\u0435\u043C\u0435\u043D\u0438");
		button_11.setMargin(new Insets(2, 2, 2, 2));
		button_11.setFont(new Font("Tahoma", Font.PLAIN, 12));
		button_11.setBounds(159, 359, 17, 24);
		frame.getContentPane().add(button_11);

		label_15 = new JLabel("");
		label_15.setForeground(Color.RED);
		label_15.setFont(new Font("Monospaced", Font.BOLD, 29));
		label_15.setBorder(new LineBorder(Color.WHITE));
		label_15.setBounds(462, 386, 91, 33);
		frame.getContentPane().add(label_15);

		textField_4 = new JTextField();
		textField_4.setDocument(new PlainDocument() {
			private static final long serialVersionUID = 1L;

			@Override
			public void insertString(int offset, String str, javax.swing.text.AttributeSet attr)
					throws BadLocationException {
				if (str == null)
					return;
				if ((getLength() + str.length()) <= 2) {
					// ����� ���� ������ �����
					if (checkString(str)) {
						super.insertString(offset, str, attr);
					}
				}
			}
		});
		textField_4.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				String text = textField_4.getText();
				if (!text.equals("")) {
					sportTab.setPenaltyNumber(4, Integer.parseInt(text));
				} else
					sportTab.setPenaltyNumber(4, -1);

			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				sportTab.setPenaltyNumber(4, Integer.parseInt(textField_4.getText()));
			}

			@Override
			public void changedUpdate(DocumentEvent arg0) {
				System.out.println("changedUpdate");
			}
		});
		textField_4.setForeground(Color.RED);
		textField_4.setFont(new Font("Monospaced", Font.BOLD, 24));
		textField_4.setColumns(10);
		textField_4.setCaretColor(Color.RED);
		textField_4.setBackground(Color.BLACK);
		textField_4.setBounds(387, 386, 67, 33);
		frame.getContentPane().add(textField_4);

		JLabel label_16 = new JLabel("\u041D\u043E\u043C\u0435\u0440");
		label_16.setForeground(Color.WHITE);
		label_16.setBackground(Color.WHITE);
		label_16.setBounds(387, 367, 37, 16);
		frame.getContentPane().add(label_16);

		comboBox_5 = new JComboBox<String>();
		comboBox_5.setToolTipText("\u041F\u0440\u0430\u0432\u044B\u043C \u043A\u043B\u0438\u043A\u043E\u043C \u043C\u043E\u0436\u043D\u043E \u0432\u0432\u0435\u0441\u0442\u0438 \u0432\u0440\u0435\u043C\u044F \u0432\u0440\u0443\u0447\u043D\u0443\u044E");
		comboBox_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String s = comboBox_5.getSelectedItem().toString();
				label_15.setText(s);
				if (!s.equals("")) {
					String s1, s2;
					s1 = s.substring(0, 2);
					s2 = s.substring(3, 5);
					int min = Integer.parseInt(s1), sec = Integer.parseInt(s2);
					sportTab.setPenaltyTime(4, (min * 60) + sec);
				} else {
					sportTab.setPenaltyTime(4, 0);
					sportTab.clearPenaltyTime(4);
				}
			}
		});
		JPopupMenu popupMenu4 = new JPopupMenu();
		JMenuItem handInputItem4 = new JMenuItem("������ �������");
		handInputItem4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int i = showHandTimeInputDialog("�������� �����");
				if (i >= 0) {
					timePenalty(4, i);
					sportTab.setPenaltyTime(4, i);
				}
			}
		});
		popupMenu4.add(handInputItem4);
		comboBox_5.setComponentPopupMenu(popupMenu4);
		comboBox_5.setModel(new DefaultComboBoxModel<>(initPenaltyTime));
		comboBox_5.setBounds(462, 359, 64, 24);
		frame.getContentPane().add(comboBox_5);

		JButton button_12 = new JButton("X");
		button_12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField_4.setText("");
				comboBox_5.setSelectedItem("");
			}
		});
		button_12
				.setToolTipText("\u0421\u0431\u0440\u043E\u0441 \u0448\u0442\u0440\u0430\u0444\u043D\u043E\u0433\u043E \u043D\u043E\u043C\u0435\u0440\u0430 \u0438 \u0435\u0433\u043E \u0432\u0440\u0435\u043C\u0435\u043D\u0438");
		button_12.setMargin(new Insets(2, 2, 2, 2));
		button_12.setFont(new Font("Tahoma", Font.PLAIN, 12));
		button_12.setBounds(536, 359, 17, 24);
		frame.getContentPane().add(button_12);
		
		JCheckBox checkBoxToFile = new JCheckBox("\u0412 \u0444\u0430\u0439\u043B");
		checkBoxToFile.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange()==ItemEvent.SELECTED) {
					System.out.println("����");
					valuesToFile.start();
				}else {
					valuesToFile.stop();
				}
				
			}
		});
		checkBoxToFile.setForeground(Color.WHITE);
		checkBoxToFile.setBackground(Color.BLACK);
		checkBoxToFile.setBounds(482, 6, 71, 25);
		frame.getContentPane().add(checkBoxToFile);
		// ������������� �����
		clip = Applet.newAudioClip(urlSound);
		// initSportTabObject();
	}

	// �������� ������� sportTab
	private void initSportTabObject() {
		if (sportTab != null) {
			sportTab.close();
		}
		if (sportTab == null) {
			if (comboBox.getSelectedItem().toString() != "" & comboBox_1.getSelectedItem().toString() != "") {
				SerialPort serialPortToString = getSerialPort(comboBox.getSelectedItem().toString());
				SerialPort serialPortToTab = getSerialPort(comboBox_1.getSelectedItem().toString());
				if (serialPortToString != null & serialPortToTab != null) {
					sportTab = new SportTabCls(serialPortToString, serialPortToTab);
					sportTab.setListener(this);
				} else {
					JOptionPane.showMessageDialog(null, "����� �� �� COM ������ �����");
					if (serialPortToString != null)
						serialPortToString.close();
					if (serialPortToTab != null)
						serialPortToTab.close();
				}
			}
		}
	}

	/** ��������� SerialPort �� ����� ����� */
	private SerialPort getSerialPort(String defaultPort) {
		Enumeration<?> portList;
		CommPortIdentifier portId;

		portList = CommPortIdentifier.getPortIdentifiers();
		while (portList.hasMoreElements()) {
			portId = (CommPortIdentifier) portList.nextElement();
			if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
				if (portId.getName().equals(defaultPort)) {
					System.out.println("������ ����:" + defaultPort);
					try {
						return (SerialPort) portId.open("SimpleWrite", 2000);
					} catch (PortInUseException e) {
						System.out.println(defaultPort + " ��� ������������");
						return null;
					}
				}
			}
		}
		return null;
	}

	/** ��������� ������ �������������� � ������� COM ������ */
	public Vector<String> getArrayComPorts() {
		Enumeration<?> portList;
		CommPortIdentifier portId;
		Vector<String> ret = new Vector<String>();

		portList = CommPortIdentifier.getPortIdentifiers();
		while (portList.hasMoreElements()) {
			portId = (CommPortIdentifier) portList.nextElement();
			if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
				ret.add(portId.getName());
			}
		}
		return ret;
	}

	/**
	 * 
	 * @return windows ��� linux
	 */
	private String getOS() {
		String os = System.getProperty("os.name").toLowerCase();
		if (os.indexOf("win") >= 0)
			return "windows";
		else if (os.indexOf("nux") >= 0)
			return "linux";
		return null;
	}

	private boolean checkString(String string) {
		try {
			Integer.parseInt(string);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * ����������� ������� ����� �������
	 * @return ret- ����� ����� � ��������.
	 *         ��� -1 � ������ ������������� ����� ��� �������� ����
	 */
	private int showHandTimeInputDialog(String nameWindow) {
		int ret;

		HandInputTime hit = new HandInputTime();
		int res = JOptionPane.showConfirmDialog(null, hit, nameWindow, JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.QUESTION_MESSAGE);
		
		if (res == JOptionPane.OK_OPTION) {
			if (hit.getValue().length() == 5) {
				String s = hit.getValue();
				System.out.println("Data entered: first name=" + s);
				String s1, s2;
				s1 = s.substring(0, 2);
				s2 = s.substring(3, 5);
				int min = Integer.parseInt(s1), sec = Integer.parseInt(s2);
				ret = (min * 60) + sec;
				return ret;
			}
			return -1;
		} else {
			System.out.println("Input Canceled");
			return -1;
		}

	}

	@Override
	public void endOfPeriodListener() {
		// TODO Auto-generated method stub
		// File file = new File("C:/Windows/Media/tada.wav");

		// clip = Applet.newAudioClip(file.toURI().toURL());

		clip.play();
		toggleButton.setEnabled(false);
	}

	@Override
	public void mainTimeListener(int i) {
		// TODO Auto-generated method stub

		String s;
		s = secToTimeString(i);
		lblMainTimer.setText(s);
	}

	@Override
	public void timePenalty(int numPenalty, int time) {
		switch (numPenalty) {
		case 1:
			label_10.setText(secToTimeString(time));
			break;
		case 2:
			label_13.setText(secToTimeString(time));
			break;
		case 3:
			label_11.setText(secToTimeString(time));
			break;
		case 4:
			label_15.setText(secToTimeString(time));
			break;

		default:
			break;
		}
	}
	
	 /**
	  * �������������� time ������ � ����������������� ������ ������� ��:��
	  * @param time ����� � ��������
	  * @return String �������  ��:��
	  */
	public String secToTimeString(int time){
		String ret;
		String min4 = Integer.toString(time / 60).length() < 2 ? "0" + Integer.toString(time / 60)
				: Integer.toString(time / 60),
		sec4 = Integer.toString(time % 60).length() < 2 ? "0" + Integer.toString(time % 60) : Integer
				.toString(time % 60);
		ret= (min4 + ":" + sec4);
		return ret;
	}
	/*
	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "SwingAction");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}*/
}
