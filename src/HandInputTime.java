import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class HandInputTime extends JPanel {

	private static final long serialVersionUID = 1L;
	private JTextField firstName;

	/**
	 * Create the panel.
	 */
	public HandInputTime() {
		super(new GridBagLayout());
		firstName = new JTextField(5);
		firstName.setDocument(new PlainDocument() {
			private static final long serialVersionUID = 1L;

			@Override
			public void insertString(int offset, String str, javax.swing.text.AttributeSet attr)
					throws BadLocationException {
				if (str == null)
					return;
				if ((getLength() + str.length()) <= 5) {
					// ����� ���� ������ �����
					if (checkString(str)) {
						// ����� ����� ������� ������� ��������� ���������
						if ((getLength() + str.length()) == 2) {
							super.insertString(offset, str + ":", attr);
						} else
							super.insertString(offset, str, attr);
					}
				}
			}
		});
		firstName.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				String text = firstName.getText();
				if (!text.equals("")) {
					// sportTab.setPenaltyNumber(1, Integer.parseInt(text));
				} else {
					// sportTab.setPenaltyNumber(1, -1);
				}
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				// sportTab.setPenaltyNumber(1,
				// Integer.parseInt(firstName.getText()));
			}

			@Override
			public void changedUpdate(DocumentEvent arg0) {
				// System.out.println("changedUpdate");
			}
		});

		JLabel label = new JLabel("������� ����� � �������: ��:��  ������: 00:35");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(0, 0, 5, 0);
		gbc_label.gridx = 1;
		gbc_label.gridy = 0;
		add(label, gbc_label);
		add(firstName, new GridBagConstraints(1, 1, 1, 1, 1, 0, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		// firstName.requestFocus();
		//��������� ������ ��� ����������. P������ ����� ��: http://tips4java.wordpress.com/2010/03/14/dialog-focus/
		firstName.addAncestorListener( new RequestFocusListener() );
	}

	public String getValue() {
		return firstName.getText();
	}

	private boolean checkString(String string) {
		try {
			Integer.parseInt(string);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	//����� ��������� �������� ����� ��� ���������� P������ ����� ��: http://tips4java.wordpress.com/2010/03/14/dialog-focus/
	public class RequestFocusListener implements AncestorListener {
		private boolean removeListener;

		/*
		 * Convenience constructor. The listener is only used once and then it
		 * is removed from the component.
		 */
		public RequestFocusListener() {
			this(true);
		}

		/*
		 * Constructor that controls whether this listen can be used once or
		 * multiple times.
		 * 
		 * @param removeListener when true this listener is only invoked once
		 * otherwise it can be invoked multiple times.
		 */
		public RequestFocusListener(boolean removeListener) {
			this.removeListener = removeListener;
		}

		@Override
		public void ancestorAdded(AncestorEvent e) {
			JComponent component = e.getComponent();
			component.requestFocusInWindow();

			if (removeListener)
				component.removeAncestorListener(this);
		}

		@Override
		public void ancestorMoved(AncestorEvent e) {
		}

		@Override
		public void ancestorRemoved(AncestorEvent e) {
		}
	}

}
