import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JOptionPane;

public class SportTabCls {

	private SerialPort serialPortToString, serialPortToTab;
	OutputStream outputStreamToString = null, outputStreamToTab = null;
	int mainTimerInt;
	int leftUpPenaltyTimeInt = 0, leftDownPenaltyTimeInt = 0, rightUpPenaltyTimeInt = 0,
			rightDownPenaltyTimeInt = 0;
	/** ����� ������� � �������� */
	int periodTime;
	// MainTimer_TimerTask mainTimer_TimerTask;
	MainTimerMyTimer mainTimerMyTimer = null;
	Timer mainTimerT = null;
	PenaltyTimer luPenaltyT = null, ldPenaltyT = null, ruPenaltyT = null, rdPenaltyT = null;
	boolean isStarted = false;
	SportTabClsEvents sportTabClsEvents;
	/**
	 * ���������� ��� ����������� ������� ������� ���������� ������� ����������
	 * �� ������� ���������� �������
	 */
	long remainingMainTimer, lastStartingMainTimer;
	long remainingLUPenalty, lastStartingLUPenalty;
	// ���������� �������� ������� ������������ �� �����
	private volatile byte leftGoal = 0x30;
	private volatile byte rightGoal = 0x30;
	private volatile byte period = 0x30;
	private byte[] leftUpPenalty = { 0x20, 0x20 };
	private byte[] leftDownPenalty = { 0x20, 0x20 };
	private byte[] rightUpPenalty = { 0x20, 0x20 };
	private byte[] rightDownPenalty = { 0x20, 0x20 };
	private byte[] leftUpPenaltyTime = { 0x20, 0x20, 0x20 };
	private byte[] leftDownPenaltyTime = { 0x20, 0x20, 0x20 };
	private byte[] rightUpPenaltyTime = { 0x20, 0x20, 0x20 };
	private byte[] rightDownPenaltyTime = { 0x20, 0x20, 0x20 };
	private byte[] mainTimer = { 0x20, 0x30, 0x30, 0x30 };

	public SportTabCls(SerialPort toString, SerialPort toTab) {
		this.serialPortToString = toString;
		this.serialPortToTab = toTab;
		// ������������� com ������
		try {
			outputStreamToString = this.serialPortToString.getOutputStream();
			outputStreamToTab = this.serialPortToTab.getOutputStream();
		} catch (IOException e) {
			System.out.println("�� ������� �������� OutputStream");
			// e.printStackTrace();
		}
		// ��������� ������
		try {
			this.serialPortToString.setSerialPortParams(19200, SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
					SerialPort.PARITY_NONE);
			this.serialPortToTab.setSerialPortParams(19200, SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
					SerialPort.PARITY_NONE);
		} catch (UnsupportedCommOperationException e) {
		}
		// ������ ������� �������� �� �����
		MainTimerTask mainTimerTask = new MainTimerTask();
		Timer mainTimerToTab = new Timer();
		mainTimerToTab.schedule(mainTimerTask, 0, 60);

		mainTimerInt = 0;
		remainingMainTimer = 0;
		remainingLUPenalty = 1000;
		mainTimerMyTimer = new MainTimerMyTimer(1000, 1000);
	}

	public void setLeftGoal(int goal) {
		leftGoal = (byte) (goal + 0x30);
	}

	public void setRightGoal(int goal) {
		rightGoal = (byte) (goal + 0x30);
	}

	public void setPeriod(int per) {
		period = (byte) (per + 0x30);
	}

	/**
	 * ��������� ������ ���������
	 * 
	 * @param pos
	 *            1-����� �������, 2-����� ������, 3-������ �������, 4-������
	 *            ������
	 * @param number
	 *            ����� ��������� ��� -1 ��� �������
	 */
	public void setPenaltyNumber(int pos, int number) {
		switch (pos) {
		case 1:
			if (number >= 0) {
				leftUpPenalty[0] = (byte) (number / 10 == 0 ? 0x20 : (number / 10) + 0x30);
				leftUpPenalty[1] = (byte) ((number % 10) + 0x30);
			} else {
				leftUpPenalty[0] = 0x20;
				leftUpPenalty[1] = 0x20;
			}
			break;
		case 2:
			if (number >= 0) {
				leftDownPenalty[0] = (byte) (number / 10 == 0 ? 0x20 : (number / 10) + 0x30);
				leftDownPenalty[1] = (byte) ((number % 10) + 0x30);
			} else {
				leftDownPenalty[0] = 0x20;
				leftDownPenalty[1] = 0x20;
			}
			break;
		case 3:
			if (number >= 0) {
				rightUpPenalty[0] = (byte) (number / 10 == 0 ? 0x20 : (number / 10) + 0x30);
				rightUpPenalty[1] = (byte) ((number % 10) + 0x30);
			} else {
				rightUpPenalty[0] = 0x20;
				rightUpPenalty[1] = 0x20;
			}
			break;
		case 4:
			if (number >= 0) {
				rightDownPenalty[0] = (byte) (number / 10 == 0 ? 0x20 : (number / 10) + 0x30);
				rightDownPenalty[1] = (byte) ((number % 10) + 0x30);
			} else {
				rightDownPenalty[0] = 0x20;
				rightDownPenalty[1] = 0x20;
			}
			break;

		default:
			break;
		}

	}

	/**
	 * ��������� ������� ����������
	 * 
	 * @param pos
	 *            1-����� �������, 2-����� ������, 3-������ �������, 4-������
	 *            ������
	 * @param time
	 *            ����� � ��������
	 */
	public void setPenaltyTime(int pos, int time) {
		switch (pos) {
		case 1:
			leftUpPenaltyTimeInt = time;
			leftUpPenaltyTime = intTo3Byte(leftUpPenaltyTimeInt);
			break;
		case 2:
			leftDownPenaltyTimeInt = time;
			leftDownPenaltyTime = intTo3Byte(leftDownPenaltyTimeInt);
			break;
		case 3:
			rightUpPenaltyTimeInt = time;
			rightUpPenaltyTime = intTo3Byte(rightUpPenaltyTimeInt);
			break;
		case 4:
			rightDownPenaltyTimeInt = time;
			rightDownPenaltyTime = intTo3Byte(rightDownPenaltyTimeInt);
			break;

		default:
			break;
		}

	}

	/**
	 * ������� ������� ��������� �� �����. ����������: �������� �������
	 * ���������� �� ���������� �.�. ���� � ��������� ���� ������������� �����
	 * �� ��� ��������� ����� �������� �� ����� �������� ��������� �� ����������
	 * � �������������. ��� ������ ������� �������� ����� ��� ��
	 * ����������������� ����� ������� ������� setPenaltyTime(int pos, int time)
	 * ��� time=0 ����� ������ �����.
	 * 
	 * @param pos
	 *            1-����� �������, 2-����� ������, 3-������ �������, 4-������
	 *            ������
	 */
	public void clearPenaltyTime(int pos) {
		switch (pos) {
		case 1:
			leftUpPenaltyTime = intTo3Byte(-1);
			break;
		case 2:
			leftDownPenaltyTime = intTo3Byte(-1);
			break;
		case 3:
			rightUpPenaltyTime = intTo3Byte(-1);
			break;
		case 4:
			rightDownPenaltyTime = intTo3Byte(-1);
			break;

		default:
			break;
		}
	}

	/** ������ ��������� ������� */
	public void timerStart() {
		// mainTimer_TimerTask = new MainTimer_TimerTask();
		// mainTimerT = new Timer();
		// mainTimerT.schedule(mainTimer_TimerTask, 1000 - remainingMainTimer,
		// 1000);

		mainTimerMyTimer.start();

		if (leftUpPenaltyTimeInt > 0) {
			if (luPenaltyT == null) {
				luPenaltyT = new PenaltyTimer(1, 1000, 1000);
			}
			luPenaltyT.start();
		}
		if (leftDownPenaltyTimeInt > 0) {
			if (ldPenaltyT == null) {
				ldPenaltyT = new PenaltyTimer(2, 1000, 1000);
			}
			ldPenaltyT.start();
		}
		if (rightUpPenaltyTimeInt > 0) {
			if (ruPenaltyT == null) {
				ruPenaltyT = new PenaltyTimer(3, 1000, 1000);
			}
			ruPenaltyT.start();
		}
		if (rightDownPenaltyTimeInt > 0) {
			if (rdPenaltyT == null) {
				rdPenaltyT = new PenaltyTimer(4, 1000, 1000);
			}
			rdPenaltyT.start();
		}
	}

	/** ��������� ��������� ������� */
	public void timerStop() {
		long curentTimeMillis = System.currentTimeMillis();
		remainingMainTimer = curentTimeMillis - lastStartingMainTimer;
		remainingLUPenalty = curentTimeMillis - lastStartingLUPenalty;
		// mainTimerT.cancel();
		mainTimerMyTimer.pause();
		if (leftUpPenaltyTimeInt > 0 & luPenaltyT != null) {
			luPenaltyT.pause();
		}
		if (leftDownPenaltyTimeInt > 0 & ldPenaltyT != null) {
			ldPenaltyT.pause();
		}
		if (rightUpPenaltyTimeInt > 0 & ruPenaltyT != null) {
			ruPenaltyT.pause();
		}
		if (rightDownPenaltyTimeInt > 0 & rdPenaltyT != null) {
			rdPenaltyT.pause();
		}
	}

	/** ����� ��������� ������� */
	public void timerReset() {
		if (mainTimerMyTimer != null) {
			mainTimerMyTimer.pause();
			mainTimerMyTimer.reset();
			mainTimer = intTo4Byte(-1);
			mainTimerInt = 0;
			remainingMainTimer = 0;
			sportTabClsEvents.mainTimeListener(mainTimerInt);
		}

	}

	/** ��������� ������� ������� � �������� */
	public void setPeriodTime(int periodTime) {
		this.periodTime = periodTime;
	}
	
	public void setInitialTime(int i){
		mainTimerInt =i;
		mainTimer = intTo4Byte(mainTimerInt);
		sportTabClsEvents.mainTimeListener(mainTimerInt);
	}

	/** ���������� �� ������ */
	public void setToString(String s) {
		// �������������� ������
		// ���������� �������� ���� ������ 24 ��������
		// �������� ������ ���� ������ 24 �������
		String strFormated = String.format("%-24.24s", s);
		int[] dest = new int[56];
		// ������������� ������������� ����������
		int[] tmp1 = { 0x02, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,
				0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x10, 0x92, 0x03, 0x02 };
		int[] tmp2 = stringToBytes(strFormated);
		int[] tmp3 = { 0x10, calculateCheckSumString(tmp2), 0x03 };
		// ������������� �������������� ����������
		for (int i = 0; i < 29; i++) {
			dest[i] = tmp1[i];
		}
		for (int i = 29; i < 53; i++) {
			dest[i] = tmp2[i - 29];
		}
		for (int i = 53; i < 56; i++) {
			dest[i] = tmp3[i - 53];
		}
		// ��������
		sendToString(dest);
	}

	/** �������� �� ������ */
	private void sendToString(int[] str) {

		// ����������� int[] � byte[]
		byte[] dest1 = new byte[28];
		byte[] dest2 = new byte[28];
		for (int i = 0; i < 28; i++) {
			dest1[i] = (byte) str[i];
		}
		for (int i = 0; i < 28; i++) {
			dest2[i] = (byte) str[i + 28];
		}

		// ��������
		try {
			outputStreamToString.write(dest1);
			Thread.sleep(60);
			outputStreamToString.write(dest2);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
		}
		// ����� �������
		// serialPortToString.close();
	}

	/**
	 * 
	 * @param i
	 *            ����� � �������� ��� -1 ��� ��������� ��������(0x20)
	 * @return
	 */
	private byte[] intTo3Byte(int i) {
		byte[] ret = new byte[3];
		if (i >= 0) {
			ret[0] = (byte) ((i / 60) + 0x30);
			ret[1] = (byte) (((i / 10) % 6) + 0x30);
			ret[2] = (byte) ((i % 10) + 0x30);
			return ret;
		} else {
			ret[0] = 0x20;
			ret[1] = 0x20;
			ret[2] = 0x20;
			return ret;
		}
	}

	/**
	 * 
	 * @param i
	 *            ����� � �������� ��� -1 ��� ��������� ��������(0x20)
	 * @return
	 */
	private byte[] intTo4Byte(int i) {
		byte[] ret = new byte[4];
		if (i >= 0) {
			ret[0] = (byte) (((i / 600) % 6) + 0x30);
			ret[1] = (byte) ((i / 60) % 10 + 0x30);
			ret[2] = (byte) (((i / 10) % 6) + 0x30);
			ret[3] = (byte) ((i % 10) + 0x30);
			return ret;
		} else {
			ret[0] = 0x20;
			ret[1] = 0x30;
			ret[2] = 0x30;
			ret[3] = 0x30;
			return ret;
		}
	}

	public void close() {
		serialPortToString.close();
		serialPortToTab.close();
	}

	/** ���������� ����������� ����� �� ������ */
	private int calculateCheckSumString(int[] b) {
		int tmp = 0;
		for (int i = 0; i < b.length; i++) {
			tmp = tmp + b[i] - 0x20;
		}
		tmp = (tmp + 0x92) % 256;
		return tmp < 0x80 ? tmp + 0x80 : tmp;
	}

	/** ���������� ����������� ����� �� ����� */
	private static int calculateCheckSumTab(int[] b) {
		int tmp = 0;
		for (int i = 0; i < b.length; i++) {
			tmp = tmp + b[i] - 0x20;
		}
		tmp = (tmp + 0x75) % 256;
		return tmp < 0x80 ? tmp + 0x80 : tmp;
	}

	private static int[] stringToBytes(String str) {
		char[] buffer = str.toCharArray();
		int[] res = new int[buffer.length];
		for (int i = 0; i < res.length; i++) {
			try {
				res[i] = (int) str.getBytes("cp1251")[i] < 0 ? str.getBytes("cp1251")[i] + 256 : str
						.getBytes("cp1251")[i];
			} catch (UnsupportedEncodingException e) {
			}
		}
		return res;
	}

	abstract class MyTimer {
		private long delay, saveDelay, period;
		private MyTimer mt = this;
		private long forTick, forStart;
		private Timer timer;

		/**
		 * 
		 * @param d
		 *            -�������� ����� �������
		 * @param p
		 *            -������������� ������������
		 */
		MyTimer(long d, long p) {
			delay = d;
			saveDelay = d;
			period = p;
			forStart = 0;
		}

		public void pause() {
			forStart = System.currentTimeMillis() - forTick;
			try{
			timer.cancel();
			}catch(NullPointerException e){}
		}

		/**
		 * ����� �������, �.�. �������� ����� ������� ������������ ����������
		 * ����� ����� ��� ���� ���������� ��� �������� �������.
		 */
		public void reset() {
			delay = saveDelay;
			forStart = 0;
		}

		abstract void run();

		public void start() {
			forTick = System.currentTimeMillis();// ��������� ����������
			timer = new Timer();
			timer.schedule(new TimerTask() {

				@Override
				public void run() {
					forTick = System.currentTimeMillis();
					mt.run();
				}
			}, delay - (forStart > delay ? delay : forStart), period);// ���������
																		// ����������
																		// ���
																		// ���
																		// �����
																		// ����
																		// ������
			delay = 1000;
		}
	}

	// ������ ���������� ������ �� �����
	class MainTimerTask extends TimerTask {
		@Override
		public void run() {
			byte[] main = new byte[31];
			int[] forCheckSum = new int[27];
			main[0] = 0x05;
			main[1] = leftGoal;
			main[2] = rightGoal;
			main[3] = period;
			main[4] = leftUpPenalty[0];
			main[5] = leftUpPenalty[1];
			main[6] = leftUpPenaltyTime[0];
			main[7] = leftUpPenaltyTime[1];
			main[8] = leftUpPenaltyTime[2];
			main[9] = leftDownPenalty[0];
			main[10] = leftDownPenalty[1];
			main[11] = leftDownPenaltyTime[0];
			main[12] = leftDownPenaltyTime[1];
			main[13] = leftDownPenaltyTime[2];
			main[14] = rightUpPenalty[0];
			main[15] = rightUpPenalty[1];
			main[16] = rightUpPenaltyTime[0];
			main[17] = rightUpPenaltyTime[1];
			main[18] = rightUpPenaltyTime[2];
			main[19] = rightDownPenalty[0];
			main[20] = rightDownPenalty[1];
			main[21] = rightDownPenaltyTime[0];
			main[22] = rightDownPenaltyTime[1];
			main[23] = rightDownPenaltyTime[2];
			main[24] = mainTimer[0];
			main[25] = mainTimer[1];
			main[26] = mainTimer[2];
			main[27] = mainTimer[3];
			for (int i = 0; i < 27; i++) {
				forCheckSum[i] = main[i + 1];
			}
			main[28] = 0x10;
			main[29] = (byte) calculateCheckSumTab(forCheckSum);// �����������
																// �����
			main[30] = 0x03;
			try {
				outputStreamToTab.write(main);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	// ������� ������ ������� �������
	class MainTimerMyTimer extends MyTimer {
		MainTimerMyTimer(long d, long p) {
			super(d, p);
		}

		@Override
		void run() {
			mainTimerInt += 1;
			mainTimer = intTo4Byte(mainTimerInt);
			sportTabClsEvents.mainTimeListener(mainTimerInt);
			if (mainTimerInt >= periodTime) {
				endOfPeriod();
			}
		}

	}

	// ������ ������� ����������
	class PenaltyTimer extends MyTimer {

		int positionPenalty;

		PenaltyTimer(int positionPenalty, long d, long p) {
			super(d, p);
			this.positionPenalty = positionPenalty;
		}

		@Override
		void run() {
			switch (positionPenalty) {
			case 1:
				// lastStartingLUPenalty = System.currentTimeMillis();
				if (leftUpPenaltyTimeInt > 0) {
					leftUpPenaltyTimeInt--;
					leftUpPenaltyTime = intTo3Byte(leftUpPenaltyTimeInt);
					sportTabClsEvents.timePenalty(1, leftUpPenaltyTimeInt);
					System.out.println("1");
				} else {
					penaltyTimerReset(1);
				}
				break;
			case 2:
				if (leftDownPenaltyTimeInt > 0) {
					leftDownPenaltyTimeInt--;
					leftDownPenaltyTime = intTo3Byte(leftDownPenaltyTimeInt);
					sportTabClsEvents.timePenalty(2, leftDownPenaltyTimeInt);
					System.out.println("2");
				} else {
					penaltyTimerReset(2);
				}
				break;
			case 3:
				if (rightUpPenaltyTimeInt > 0) {
					rightUpPenaltyTimeInt--;
					rightUpPenaltyTime = intTo3Byte(rightUpPenaltyTimeInt);
					sportTabClsEvents.timePenalty(3, rightUpPenaltyTimeInt);
					System.out.println("3");
				} else {
					penaltyTimerReset(3);
				}
				break;
			case 4:
				if (rightDownPenaltyTimeInt > 0) {
					rightDownPenaltyTimeInt--;
					rightDownPenaltyTime = intTo3Byte(rightDownPenaltyTimeInt);
					sportTabClsEvents.timePenalty(4, rightDownPenaltyTimeInt);
					System.out.println("4");
				} else {
					penaltyTimerReset(4);
				}
				break;

			default:
				break;
			}
		}

	}

	private void penaltyTimerReset(int pos) {
		switch (pos) {
		case 1:
			luPenaltyT.pause();
			luPenaltyT.reset();
			luPenaltyT = null;
			break;
		case 2:
			ldPenaltyT.pause();
			ldPenaltyT.reset();
			ldPenaltyT = null;
			break;
		case 3:
			ruPenaltyT.pause();
			ruPenaltyT.reset();
			ruPenaltyT = null;
			break;
		case 4:
			rdPenaltyT.pause();
			rdPenaltyT.reset();
			rdPenaltyT = null;
			break;

		default:
			break;
		}

	}

	// ��������� �������
	private void endOfPeriod() {
		mainTimerMyTimer.pause();
		mainTimerMyTimer.reset();
		sportTabClsEvents.endOfPeriodListener();
		if (leftUpPenaltyTimeInt > 0) {
			luPenaltyT.pause();
		}
		if (leftDownPenaltyTimeInt > 0) {
			ldPenaltyT.pause();
		}
		if (rightUpPenaltyTimeInt > 0) {
			ruPenaltyT.pause();
		}
		if (rightDownPenaltyTimeInt > 0) {
			rdPenaltyT.pause();
		}

	}

	public void setListener(SportTabClsEvents stce) {
		sportTabClsEvents = stce;
	}

	interface SportTabClsEvents {
		/**
		 * ����� �������, ����� ����������� �������� ����� � �.�.
		 */
		public void endOfPeriodListener();

		/**
		 * ����� ��������� �������
		 * 
		 * @param i
		 *            ����� � �������
		 */
		public void mainTimeListener(int i);

		/**
		 * ����� ����������
		 * 
		 * @param numPenalty
		 *            1-����� �������, 2-����� ������, 3-������ �������,
		 *            4-������ ������
		 * @param time
		 *            ���������� ����� � ��������
		 */
		public void timePenalty(int numPenalty, int time);
	}
	/**�������� ����������� ���������*/
	@SuppressWarnings("unused")
	private static boolean checkForLibrares() {
		String pathJre = System.getProperty("java.home");
		File file1 = new File(pathJre + "/bin/rxtxSerial.dll");
		boolean existsRxtxSerial = file1.exists();
		// System.out.println(file1+" "+existsRxtxSerial);

		File file2 = new File(pathJre + "/lib/ext/RXTXcomm.jar");
		boolean existsRXTXCommJ = file2.exists();
		// System.out.println(file2+" "+existsRxtxSerial);
		if (!existsRxtxSerial | !existsRXTXCommJ) {
			String message = "���������� ���������� ����������(�) :\n";
			if (!existsRxtxSerial) {
				message += file1.getAbsolutePath() + "\n";
			}
			if (!existsRXTXCommJ) {
				message += file2.getAbsolutePath() + "\n";
			}
			message += "������ ���������� ����� �������\n� �����: http://rxtx.qbang.org/\n� �������: downloads, � ����������� � ��������� �����.\n";
			message += "��� ���������� �������������� ������: RXTX-2.1-7\n������ ������ �������� ���� ��������.";
			JOptionPane.showMessageDialog(null, message);
			return false;
		}
		return true;
	}

}
